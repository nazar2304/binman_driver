package com.binman.driver.dagger;


import javax.inject.Singleton;

import com.binman.driver.dagger.module.ApiModule;
import com.binman.driver.dagger.module.ContextModule;
import com.binman.driver.dagger.module.LocalStorageModule;
import com.binman.driver.service.TrackingService;
import com.binman.driver.ui.accept_car_and_route.AcceptCarAndRoutePresenter;
import com.binman.driver.ui.cars.CarsPresenter;
import com.binman.driver.ui.login.LoginPresenter;
import com.binman.driver.ui.main.MainPresenter;
import com.binman.driver.ui.menu.BaseMenuActivity;
import com.binman.driver.ui.routes.RoutesPresenter;
import com.binman.driver.ui.splash.SplashPresenter;
import com.binman.driver.ui.welcome.WelcomePresenter;

import dagger.Component;


@Singleton
@Component(modules = {ContextModule.class, ApiModule.class, LocalStorageModule.class})
public interface AppComponent {
    void inject(LoginPresenter loginPresenter);

    void inject(SplashPresenter splashPresenter);

    void inject(WelcomePresenter welcomePresenter);

    void inject(CarsPresenter carsPresenter);

    void inject(RoutesPresenter routesPresenter);

    void inject(AcceptCarAndRoutePresenter acceptCarAndRoutePresenter);

    void inject(MainPresenter mainPresenter);

    void inject(TrackingService trackingService);

    void inject(BaseMenuActivity baseMenuActivity);
}

