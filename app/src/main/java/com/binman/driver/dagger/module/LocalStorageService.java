package com.binman.driver.dagger.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class LocalStorageService {

    private static final String SETTINGS = "settings_service";
    private Context context;

    LocalStorageService(Context context) {
        this.context = context;
    }

    public void writeString(String key, String value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putString(key, value).apply();
    }

    public String readString(String key, String defValue) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getString(key, defValue);
    }

    public void writeBoolean(String key, Boolean value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putBoolean(key, value).apply();
    }

    public Boolean readBoolean(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }

    public void writeLong(String key, Long value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putLong(key, value).apply();
    }

    public void remove(String key) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().remove(key).apply();
    }

    public long readLong(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getLong(key, 0);
    }

    public void writeInteger(String key, Integer value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putInt(key, value).apply();
    }

    public Integer readInteger(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getInt(key, 0);
    }

    public void writeFloat(String key, Float value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putFloat(key, value).apply();
    }

    public Float readFloat(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getFloat(key, 0);
    }

    public void writeObject(String key, Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).edit().putString(key, json).apply();
    }

    public <T> T readObject(String key, Class<T> type) {
        Gson gson = new Gson();
        String json = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).getString(key, "");
        Object fromJson = gson.fromJson(json, type);
        return (T) fromJson;
    }


    public void writeList(String key, Object object, Type type) {
        Gson gson = new Gson();
        String json = gson.toJson(object, type);
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).edit().putString(key, json).apply();
    }

    public <T> T readList(String key, Type type) {
        Gson gson = new Gson();
        String json = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).getString(key, "");
        Object fromJson = gson.fromJson(json, type);
        return (T) fromJson;
    }


    public void clearData() {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().clear().apply();
    }

    public <T> List<T> readList(String key, Class<T> type) {
        String jsonStr = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).getString(key, "");
        Gson gson = new Gson();
        List<T> json = gson.fromJson(jsonStr, new ListOfSomething<T>(type));
        if (json != null)
            return json;
        else
            return new ArrayList<>();
    }

    public <T> void writeList(String key, List<T> object, Class<T> type) {
        Type listType = new TypeToken<List<T>>() {
        }.getType();
        Gson gson = new Gson();
        String json = gson.toJson(object, listType);
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).edit().putString(key, json).apply();
    }


    class ListOfSomething<X> implements ParameterizedType {

        private Class<?> wrapped;

        ListOfSomething(Class<X> wrapped) {
            this.wrapped = wrapped;
        }

        public Type[] getActualTypeArguments() {
            return new Type[] {wrapped};
        }

        public Type getRawType() {
            return List.class;
        }

        public Type getOwnerType() {
            return null;
        }

    }
}
