package com.binman.driver.dagger.module;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ContextModule.class})
public class LocalStorageModule {

    @Provides
    @Singleton
    LocalStorage provideLocalStorage(Context context){
        return new LocalStorage(context);
    }

    @Provides
    @Singleton
    LocalStorageService provideLocalStorageService(Context context){
        return new LocalStorageService(context);
    }
}
