package com.binman.driver.dagger.module;

import javax.inject.Singleton;

import com.binman.driver.connection.Api;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;


@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }

}
