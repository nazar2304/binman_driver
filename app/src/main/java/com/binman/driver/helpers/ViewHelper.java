package com.binman.driver.helpers;

import android.graphics.Color;
import android.widget.Button;

import com.binman.driver.R;

public class ViewHelper {


    public static void setButtonColor(Button button, int status, boolean loaded) {
        if (loaded) {
            if (status == 2) {
                button.setBackgroundResource(R.drawable.background_button_green);
                button.setTextColor(Color.WHITE);
            } else {
                button.setBackgroundResource(R.drawable.background_button);
                button.setTextColor(button.getResources().getColor(R.color.active));
            }
        } else {
            if (status == 1) {
                button.setBackgroundResource(R.drawable.background_button_red);
                button.setTextColor(Color.WHITE);
            } else {
                button.setBackgroundResource(R.drawable.background_button);
                button.setTextColor(button.getResources().getColor(R.color.active));
            }
        }
    }
}
