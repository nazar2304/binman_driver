package com.binman.driver.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.binman.driver.ui.splash.SplashActivity;


public class RestartHelper {
    public static void restartApp(Context context){
        Intent mStartActivity = new Intent(context, SplashActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(mStartActivity);
    }
}
