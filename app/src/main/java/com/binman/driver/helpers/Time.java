package com.binman.driver.helpers;

import android.annotation.SuppressLint;

public class Time {

    public static int getCurrentTimeInSeconds(){
        Long sec = System.currentTimeMillis()/1000;
        return sec.intValue();
    }

    @SuppressLint("DefaultLocale")
    public static String convertSecondsToTime(long totalSecs){
        long hours = totalSecs / 3600;
        long minutes = (totalSecs % 3600) / 60;
        if (hours == 0){
            return minutes + " мин.";
        } else {
            return hours +" ч. " +  minutes +" мин.";
        }
    }
}
