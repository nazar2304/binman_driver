package com.binman.driver.helpers;

import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BaseView;
import com.binman.driver.dagger.module.LocalStorage;

public class ErrorHelper {
    public static void checkError(LocalStorage localStorage, int status, BaseView baseView){
         if (status == 4) {
             baseView.showToast("Маршрут занят");
        } else if (status == 3) {
             baseView.showToast("Машина занята");
        } else if (status == 2) {
             baseView.showToast("Ошибка авторизации");
             localStorage.clearData();
            RestartHelper.restartApp(App.getApp());
        } else {
             baseView.showToast(R.string.unknown_exception);
        }
    }
}
