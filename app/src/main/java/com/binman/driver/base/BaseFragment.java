package com.binman.driver.base;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.Objects;

import butterknife.Unbinder;

public class BaseFragment extends AndroidxFragment implements BaseView {
    protected static final String TAG = "myLog";

    protected Unbinder unbinder;


    public void showToast(int id) {
        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean status) {
        if (getActivity() != null)
            if (status)
                ProgressDialog.getInstance().show(getActivity().getSupportFragmentManager(), "progress");
            else
                ProgressDialog.getInstance().dismiss();
    }

    @Override
    public void hideKeyboard(View view) {

        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view == null) {
            view = new View(getActivity());
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unbinder.unbind();
        } catch (Exception ignored) {

        }

    }
}
