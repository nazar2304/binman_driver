package com.binman.driver.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.dagger.module.LocalStorageService;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.GPS;
import com.binman.driver.models.User;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrackingService extends Service {
    @Inject LocalStorageService mLocalStorage;
    @Inject Api mApi;
    List<GPS> gpsList = new ArrayList<>();
    LocationRequest mLocationRequest;
    Disposable disposable;
    FusedLocationProviderClient mFusedLocationClient;

    public TrackingService() {
        App.getAppComponent().inject(this);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "TRACKING")
                .setSmallIcon(R.drawable.symbol);
        Notification notification = builder
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle("Активно приложение Binman")
                .build();

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel("TRACKING", "TRACKING", NotificationManager.IMPORTANCE_MIN);
            channel.setDescription("TRACKING");
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(channel);
        }
        startForeground(1231, notification);
        buildGoogleApiClient();
        saveLocation();
        sendTrackingData();

        return START_NOT_STICKY;
    }


    void sendTrackingData() {
        send();
        new Handler().postDelayed(this::sendTrackingData, 60000);
    }


    void send() {
        List<GPS> currentGpsList = mLocalStorage.readList("gpsList", GPS.class);
        if (currentGpsList.size() == 0)
            return;
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", new LocalStorage(App.getApp()).readObject("login", User.class).getToken());
        fields.put("entity", "coords");
        fields.put("lat", currentGpsList.get(0).getLatitude());
        fields.put("lon", currentGpsList.get(0).getLongitude());
        fields.put("timestamp", Time.getCurrentTimeInSeconds());

        disposable = mApi.sendPoint(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                List<GPS> list = mLocalStorage.readList("gpsList", GPS.class);
                                list.remove(0);
                                mLocalStorage.writeList("gpsList", list, GPS.class);
                                send();
                            }
                        }
                        , throwable -> {});
    }

    protected synchronized void buildGoogleApiClient() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(70000);
        mLocationRequest.setFastestInterval(65000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    private void saveLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (lm != null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                Location location = locationList.get(locationList.size() - 1);
                gpsList = mLocalStorage.readList("gpsList", GPS.class);
                gpsList.add(new GPS(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude())));
                mLocalStorage.writeList("gpsList", gpsList, GPS.class);
                Intent in = new Intent("track_position");
                in.putExtra("lat", location.getLatitude());
                in.putExtra("lon", location.getLongitude());
                sendBroadcast(in);
            }
        }
    };



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            disposable.dispose();
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        } catch (Exception ignored) {
        }

    }
}
