package com.binman.driver.ui.dialogs;

import android.app.Dialog;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.binman.driver.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloseRouteDialog extends DialogFragment {

    private ICloseListener mListener;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_close_route, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            Dialog dialog = getDialog();
            if (dialog != null && dialog.getWindow() != null) {
                dialog.getWindow().setLayout(size.x, size.y);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        }
    }

    @OnClick(R.id.endRoute)
    void onClickEndRoute(){
        mListener.onEndRoute();
        dismiss();
    }

    @OnClick(R.id.endDay)
    void onClickEndDay(){
        mListener.onEndDay();
        dismiss();
    }

    @OnClick(R.id.cancel)
    void onClickCancel(){
        dismiss();
    }

    public void setListener(ICloseListener listener) {
        mListener = listener;
    }

    public interface ICloseListener{
        void onEndRoute();
        void onEndDay();
    }
}

