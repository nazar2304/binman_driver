package com.binman.driver.ui.main.map;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binman.driver.R;
import com.binman.driver.base.BaseRecyclerAdapter;
import com.binman.driver.helpers.ViewHelper;
import com.binman.driver.models.Container;
import com.binman.driver.ui.main.IClickListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ContainerAdapter extends BaseRecyclerAdapter<Container> {

    private IClickListener mListener;
    private IDataChangeListener mIDataChangeListener;

    ContainerAdapter(List<Container> containers) {
        items = containers;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ContainerAdapter.ContainerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_container, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ContainerAdapter.ContainerViewHolder) holder).bindView(position);
    }

    public class ContainerViewHolder extends RecyclerView.ViewHolder {

        TextView mTitle;
        TextView mSubtitle;
        ImageView mIcon;
        Button notLoaded;
        Button mLoaded;

        ContainerViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title);
            mSubtitle = itemView.findViewById(R.id.subtitle);
            mIcon = itemView.findViewById(R.id.icon);
            notLoaded = itemView.findViewById(R.id.notLoaded);
            mLoaded = itemView.findViewById(R.id.loaded);
        }

        void bindView(int position) {
            Container container = items.get(position);
            mTitle.setText(container.getName());
            mSubtitle.setText(String.format(mSubtitle.getResources().getString(R.string.volume_placeholder), String.valueOf(container.getVolume())));
            Glide.with(mIcon).load(container.getImage()).into(mIcon);
            ViewHelper.setButtonColor(mLoaded, container.getStatus(), true);
            ViewHelper.setButtonColor(notLoaded, container.getStatus(), false);

            mLoaded.setOnClickListener(v -> {
                List<Container> cont = new ArrayList<>();
                cont.add(container);
                mListener.onClickLoad(cont, 2, () -> {
                    container.setStatus(2);
                    notifyDataSetChanged();
                    mIDataChangeListener.onDataChanged();
                });
            });
            notLoaded.setOnClickListener(v -> {
                List<Container> cont = new ArrayList<>();
                cont.add(container);
                mListener.onClickLoad(cont, 1, () -> {
                    container.setStatus(1);
                    notifyDataSetChanged();
                    mIDataChangeListener.onDataChanged();
                });
            });
        }
    }

    void setIDataChangeListener(IDataChangeListener IDataChangeListener) {
        mIDataChangeListener = IDataChangeListener;
    }

    public void setListener(IClickListener listener) {
        mListener = listener;
    }

    public interface  IDataChangeListener{
        void onDataChanged();
    }
}
