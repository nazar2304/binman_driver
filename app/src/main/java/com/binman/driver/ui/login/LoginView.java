package com.binman.driver.ui.login;

import com.binman.driver.base.BaseView;

public interface LoginView extends BaseView {
    void showErrorMsg(boolean status);
    void openMainActivity();
}
