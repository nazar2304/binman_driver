package com.binman.driver.ui.cars;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.helpers.ErrorHelper;
import com.binman.driver.helpers.RestartHelper;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.Car;
import com.binman.driver.models.Type;
import com.binman.driver.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class CarsPresenter extends BasePresenter<CarsView> {

    @Inject LocalStorage mLocalStorage;
    @Inject Api mApi;
    private List<Car> mCars = new ArrayList<>();
    private int pageForLoad = 1;
    private int allCount = 0;

    CarsPresenter() {
        App.getAppComponent().inject(this);
    }

    public List<Car> getCars() {
        return mCars;
    }

    void load() {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("entity", "cars");
        fields.put("count", 10);
        fields.put("page", pageForLoad);

        Car progressCar = new Car();
        progressCar.setType(Type.PROGRESS);
        clearLastItem();
        mCars.add(progressCar);
        getViewState().notifyList();

        Disposable disposable = mApi.getCars(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if(result.getStatus() == 0){
                                pageForLoad++;
                                clearLastItem();
                                mCars.addAll(result.getCarsList());
                                getViewState().setCarsCount(result.getAllCount());
                                allCount = result.getAllCount();
                                addShowMore();
                            } else {
                                clearLastItem();
                                addShowMore();
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                            getViewState().notifyList();
                        }
                        , throwable -> {
                            clearLastItem();
                            addShowMore();
                            getViewState().notifyList();
                            getViewState().showToast(throwable.getMessage());
                        });
        unsubscribeOnDestroy(disposable);
    }

    void selectCar(Car car) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("car_id", car.getId());
        fields.put("timestamp", Time.getCurrentTimeInSeconds());

        getViewState().showProgress(true);

        Disposable disposable = mApi.takeCar(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                mLocalStorage.writeObject("selected_car", car);
                                for (int i = 0; i < mCars.size(); i++) {
                                    if (mCars.get(i).getUser() != null && mCars.get(i).getUser().getId() == mLocalStorage.readObject("login", User.class).getId())
                                        mCars.get(i).setUser(null);
                                    if (mCars.get(i).getId() == car.getId())
                                        mCars.get(i).setUser(mLocalStorage.readObject("login", User.class));
                                }
                                getViewState().notifyList();
                                if (mLocalStorage.readBoolean("change")) {
                                    getViewState().openAcceptCarAndRoute();
                                    mLocalStorage.writeBoolean("change", false);
                                } else {
                                    getViewState().openRoutes();
                                }
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                            getViewState().showProgress(false);
                        }
                        , throwable -> {
                            getViewState().showToast(throwable.getMessage());
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    private void clearLastItem() {
        if (mCars.size() > 0 && mCars.get(mCars.size() - 1).getType() != Type.STANDART)
            mCars.remove(mCars.size() - 1);
    }

    private void addShowMore() {
        if (mCars.size() < allCount) {
            Car showMore = new Car();
            showMore.setType(Type.SHOW_MORE);
            mCars.add(showMore);
        }
    }
}
