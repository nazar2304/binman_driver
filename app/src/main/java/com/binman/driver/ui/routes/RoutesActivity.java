package com.binman.driver.ui.routes;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.R;
import com.binman.driver.models.Route;
import com.binman.driver.ui.menu.BaseMenuActivity;
import com.binman.driver.ui.accept_car_and_route.AcceptCarAndRouteActivity;
import com.binman.driver.ui.cars.CarsActivity;
import com.binman.driver.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoutesActivity extends BaseMenuActivity implements RoutesView {

    @BindView(R.id.list) RecyclerView mList;
    private RouteAdapter mAdapter;
    @InjectPresenter RoutesPresenter mPresenter;
    @BindView(R.id.routesCount) TextView mRoutesCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        unbinder = ButterKnife.bind(this);

        mAdapter = new RouteAdapter(mPresenter.getRoutes());
        mAdapter.setListener(new RouteAdapter.IClickListener() {
            @Override
            public void onClickSelectRoute(Route route) {
                mPresenter.selectRoute(route);
            }

            @Override
            public void onClickShowMore() {
                mPresenter.load();
            }
        });
        mList.setAdapter(mAdapter);

        mPresenter.load();
    }

    @OnClick(R.id.routesCount)
    protected void onClickList() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.backToSelectCar)
    protected void onClickBackToSelectCar() {
        startActivity(new Intent(this, CarsActivity.class));
    }

    @Override
    public void notifyList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setRouteCount(long count) {
        mRoutesCount.setText(String.format(getResources().getString(R.string.cars_count), count));
    }

    @Override
    public void openAcceptCarAndRoute() {
        startActivity(new Intent(this, AcceptCarAndRouteActivity.class));
    }
}
