package com.binman.driver.ui.routes;


import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.helpers.ErrorHelper;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.Route;
import com.binman.driver.models.Type;
import com.binman.driver.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RoutesPresenter extends BasePresenter<RoutesView> {


    @Inject LocalStorage mLocalStorage;
    @Inject Api mApi;
    private List<Route> mRoutes = new ArrayList<>();

    RoutesPresenter() {
        App.getAppComponent().inject(this);
    }

    public List<Route> getRoutes() {
        return mRoutes;
    }

    private int pageForLoad = 1;
    private long allCount = 0;


    void load() {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("entity", "routes");
        fields.put("count", 10);
        fields.put("page", pageForLoad);

        Route progressRoute = new Route();
        progressRoute.setType(Type.PROGRESS);
        clearLastItem();
        mRoutes.add(progressRoute);
        getViewState().notifyList();

        Disposable disposable = mApi.getRoutes(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                pageForLoad++;
                                clearLastItem();
                                mRoutes.addAll(result.getRoutes());
                                getViewState().setRouteCount(result.getAllCount());
                                allCount = result.getAllCount();
                                addShowMore();
                            } else {
                                clearLastItem();
                                addShowMore();
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                            getViewState().notifyList();
                        }
                        , throwable -> {
                            clearLastItem();
                            addShowMore();
                            getViewState().notifyList();
                            getViewState().showToast(throwable.getMessage());
                        });
        unsubscribeOnDestroy(disposable);
    }

    void selectRoute(Route route) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("route_id", route.getId());
        fields.put("timestamp", Time.getCurrentTimeInSeconds());
        getViewState().showProgress(true);

        Disposable disposable = mApi.selectRoute(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                mLocalStorage.writeObject("selected_route", route);
                                for (int i = 0; i < mRoutes.size(); i++) {
                                    if (mRoutes.get(i).getDrivers().size() != 0)
                                        for (User user : mRoutes.get(i).getDrivers()) {
                                            if (user.getId() == mLocalStorage.readObject("login", User.class).getId()) {
                                                mRoutes.get(i).getDrivers().remove(user);
                                            }
                                        }
                                    if (mRoutes.get(i).getId() == route.getId())
                                        mRoutes.get(i).getDrivers().add(0, mLocalStorage.readObject("login", User.class));
                                }
                                getViewState().notifyList();
                                getViewState().openAcceptCarAndRoute();
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                            getViewState().showProgress(false);
                        }
                        , throwable -> {
                            getViewState().showToast(throwable.getMessage());
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    private void clearLastItem() {
        if (mRoutes.size() > 0 && mRoutes.get(mRoutes.size() - 1).getType() != Type.STANDART)
            mRoutes.remove(mRoutes.size() - 1);
    }

    private void addShowMore() {
        if (mRoutes.size() < allCount) {
            Route showMore = new Route();
            showMore.setType(Type.SHOW_MORE);
            mRoutes.add(showMore);
        }
    }
}
