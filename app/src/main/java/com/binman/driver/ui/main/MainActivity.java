package com.binman.driver.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.helpers.RestartHelper;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.Route;
import com.binman.driver.service.TrackingService;
import com.binman.driver.ui.menu.BaseMenuActivity;
import com.binman.driver.ui.dialogs.CloseRouteDialog;
import com.binman.driver.ui.dialogs.ConfirmLackLoadingDialog;
import com.binman.driver.ui.dialogs.ConfirmLoadingDialog;
import com.binman.driver.ui.dialogs.ConfirmDriveLandfill;
import com.binman.driver.ui.main.list.ListFragment;
import com.binman.driver.ui.main.map.MapFragment;
import com.binman.driver.ui.routes.RoutesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import androidx.annotation.NonNull;

public class MainActivity extends BaseMenuActivity implements MainView {

    @InjectPresenter public MainPresenter mPresenter;

    @BindView(R.id.volumeLoaded) TextView mVolumeLoaded;
    @BindView(R.id.percent) TextView mPercent;
    @BindView(R.id.percentProgress) ProgressBar mPercentProgress;
    @BindView(R.id.areas) TextView mAreas;
    @BindView(R.id.areasProgress) ProgressBar mAreasProgress;
    @BindView(R.id.time) TextView mTime;
    @BindView(R.id.containers) TextView mContainers;
    @BindView(R.id.containerProgress) ProgressBar mContainersProgress;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.routeCurrentDistance) TextView mRouteCurrentDistance;
    @BindView(R.id.continueRoute) View mContinueRoute;
    @BindView(R.id.gpsContainer) View mContainerGps;

    ListFragment mListFragment = new ListFragment();
    MapFragment mMapFragment = new MapFragment();


    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                double lat = intent.getExtras().getDouble("lat", 0.0);
                double lon = intent.getExtras().getDouble("lon", 0.0);
                App.getApp().setLatLon(lat, lon);
                mMapFragment.setMyPosition(lat, lon);
            }
        }
    };

    private BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                mPresenter.showTurnOnGPS(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        IntentFilter intentFilter = new IntentFilter("track_position");
        registerReceiver(mBroadcastReceiver, intentFilter);
        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        changeFragment(mListFragment, "ListFragment");

        mPresenter.load();

        checkLocationPermission();
    }


    private void checkLocationPermission() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mPresenter.showTurnOnGPS(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, TrackingService.class));
            } else {
                startService(new Intent(this, TrackingService.class));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, TrackingService.class));
            } else {
                startService(new Intent(this, TrackingService.class));
            }
        } else {
            checkLocationPermission();
        }
    }

    public void showMapFragment() {
        changeFragment(mMapFragment, "MapFragment");
    }

    public void changeFragment(Fragment fragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        List<Fragment> fragments = fm.getFragments();

        for (Fragment f : fragments) {
            if (f != null && f.isVisible())
                ft.hide(f);
        }

        Fragment fragmentOld = fm.findFragmentByTag(tag);
        if (fragmentOld != null) {
            fragment = fragmentOld;
            ft.show(fragment).commit();
        } else {
            ft.add(R.id.container, fragment, tag).show(fragment).commit();
        }
    }

    @OnClick(R.id.turnOnGps)
    protected void onClickTurnOnGps() {
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    @OnClick(R.id.driveLandfill)
    protected void onCLickDriveLandfill() {
        ConfirmDriveLandfill confirmDriveLandfill = new ConfirmDriveLandfill();
        confirmDriveLandfill.setListener(() -> mPresenter.driveLandfill(1));
        confirmDriveLandfill.show(getSupportFragmentManager(), "confirmDriveLandfill");
    }

    @OnClick(R.id.endRoute)
    protected void onClickEndRoute() {
        CloseRouteDialog closeRouteDialog = new CloseRouteDialog();
        closeRouteDialog.setListener(new CloseRouteDialog.ICloseListener() {
            @Override
            public void onEndRoute() {
                mPresenter.endRoute();
            }

            @Override
            public void onEndDay() {
                mPresenter.endDay();
            }
        });
        closeRouteDialog.show(getSupportFragmentManager(), "closeRouteDialog");
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void showRoute(Route route) {
        mName.setText(route.getName());
        mPercent.setText(route.getPercent() + "% / 100%");
        mPercentProgress.setMax(100);
        mPercentProgress.setProgress(Math.round(route.getPercent()));
        mContainers.setText(route.getContainersLoadedCount() + " / " + route.getContainersCount());
        mContainersProgress.setMax(route.getContainersCount());
        mContainersProgress.setProgress(route.getContainersLoadedCount());
        mAreas.setText(route.getAreasLoadedCount() + " / " + route.getAreasCount());
        mAreasProgress.setMax(route.getAreasCount());
        mAreasProgress.setProgress(route.getAreasLoadedCount());
        mVolumeLoaded.setText(route.getVolumeLoaded() + " м³");
        mTime.setText(Time.convertSecondsToTime(route.getTime()));
        mRouteCurrentDistance.setText(route.getRun());
    }

    @Override
    public void notifyList() {
        mListFragment.notifyList();
    }

    @Override
    public void scrollToPosition(int position) {
        mListFragment.scrollToPosition(position);
    }

    @Override
    public void setAreasCount(int count) {
        mListFragment.setCount(count);
    }

    @Override
    public void showOnLandfill(boolean status) {
        mContinueRoute.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openRouteActivity() {
        startActivity(new Intent(this, RoutesActivity.class));
        finish();
    }

    @Override
    public void restartApp() {
        RestartHelper.restartApp(this);
    }

    @Override
    public void showTurnOnGPS(boolean status) {
        mContainerGps.setVisibility(!status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showConfirmLoadDialog(IConfirmListener confirmListener) {
        ConfirmLoadingDialog acceptLoadingDialog = new ConfirmLoadingDialog();
        acceptLoadingDialog.setListener(confirmListener);
        acceptLoadingDialog.show(getSupportFragmentManager(), "acceptLoadingDialog");
    }

    @Override
    public void showConfirmLackLoadDialog(IConfirmListener confirmListener) {
        ConfirmLackLoadingDialog confirmTheLackOfLoadingDialog = new ConfirmLackLoadingDialog();
        confirmTheLackOfLoadingDialog.setListener(confirmListener);
        confirmTheLackOfLoadingDialog.show(getSupportFragmentManager(), "confirmTheLackOfLoadingDialog");

    }

    @OnClick(R.id.continueRouteBtn)
    protected void onClickContinueRoute() {
        mPresenter.driveLandfill(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            stopService(new Intent(this, TrackingService.class));
            unregisterReceiver(mBroadcastReceiver);
            unregisterReceiver(mGpsSwitchStateReceiver);
        } catch (Exception ignored) {

        }
    }
}
