package com.binman.driver.ui.splash;

import com.binman.driver.base.BaseView;

public interface SplashView extends BaseView {

    void openLoginActivity();
    void openWelcomeActivity();
}
