package com.binman.driver.ui.accept_car_and_route;

import com.binman.driver.base.BaseView;
import com.binman.driver.models.Car;
import com.binman.driver.models.Route;

public interface AcceptCarAndRouteView extends BaseView {

    void openCarsActivity();

    void showCar(Car car);

    void showRoute(Route route);

    void openMainActivity();

    void showTitle(String name);
}
