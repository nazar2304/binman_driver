package com.binman.driver.ui.welcome;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.helpers.ErrorHelper;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.User;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class WelcomePresenter extends BasePresenter<WelcomeView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    WelcomePresenter() {
        App.getAppComponent().inject(this);
    }

    void load() {
        if (mLocalStorage.readBoolean("on_route_work"))
            getViewState().openMainActivity();
        getViewState().showUserName(mLocalStorage.readObject("login", User.class).getLastname() + " " + mLocalStorage.readObject("login", User.class).getName() + " " + mLocalStorage.readObject("login", User.class).getSecondname());
    }

    void clickExit() {
        mLocalStorage.clearData();
        getViewState().openLoginActivity();
    }


    void startWorkDay() {

        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("timestamp", Time.getCurrentTimeInSeconds());
        fields.put("working", 1);

        getViewState().showProgress(true);
        Disposable disposable = mApi.workDay(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            getViewState().showProgress(false);
                            if (result.getStatus() == 0) {
                                getViewState().openCarsActivity();
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                        }
                        , throwable -> {
                            getViewState().showProgress(false);
                            getViewState().showToast(throwable.getMessage());
                        });
        unsubscribeOnDestroy(disposable);
    }
}
