package com.binman.driver.ui.routes;

import com.binman.driver.base.BaseView;

public interface RoutesView extends BaseView {

    void notifyList();

    void setRouteCount(long count);

    void openAcceptCarAndRoute();
}
