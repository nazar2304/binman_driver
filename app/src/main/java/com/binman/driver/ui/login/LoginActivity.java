package com.binman.driver.ui.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.R;
import com.binman.driver.base.BaseActivity;
import com.binman.driver.ui.welcome.WelcomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.email) EditText email;
    @BindView(R.id.pass) EditText pass;
    @BindView(R.id.error_msg) TextView errorMsg;

    @InjectPresenter LoginPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
    }



    @OnClick(R.id.login)
    protected void onClickLogin() {
        mPresenter.login(email.getText().toString(), pass.getText().toString());
    }

    @OnClick(R.id.forgot)
    protected void onClickForgot() {
        String url = "http://binman.ru/cabinet/?forgot_password=yes";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.eye)
    protected void onClickEye() {
        pass.setInputType(pass.getInputType() == 129 ? 1 : 129);
    }

    @Override
    public void showErrorMsg(boolean status) {
        errorMsg.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, WelcomeActivity.class));
        finish();
    }
}
