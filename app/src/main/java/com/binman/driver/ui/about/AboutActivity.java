package com.binman.driver.ui.about;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.binman.driver.R;
import com.binman.driver.ui.menu.BaseMenuActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutActivity extends BaseMenuActivity {

    @BindView(R.id.version) TextView mVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        unbinder = ButterKnife.bind(this);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            mVersion.setText(String.format(getResources().getString(R.string.version), pInfo.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.link)
    protected void onClickLink(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://binman.ru"));
        startActivity(i);
    }
}
