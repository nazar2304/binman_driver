package com.binman.driver.ui.accept_car_and_route;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.models.Car;
import com.binman.driver.models.Route;
import com.binman.driver.models.User;

import javax.inject.Inject;

@InjectViewState
public class AcceptCarAndRoutePresenter extends BasePresenter<AcceptCarAndRouteView> {

    @Inject LocalStorage mLocalStorage;


    AcceptCarAndRoutePresenter() {
        App.getAppComponent().inject(this);
    }


    void changeCar() {
        mLocalStorage.writeBoolean("change", true);
        getViewState().openCarsActivity();
    }

    public void load() {
        getViewState().showRoute(mLocalStorage.readObject("selected_route", Route.class));
        getViewState().showCar(mLocalStorage.readObject("selected_car", Car.class));
        User user = mLocalStorage.readObject("login", User.class);
        getViewState().showTitle(user.getLastname() + " " + user.getName() + " " + user.getSecondname());
    }

    void startWork() {
        mLocalStorage.writeBoolean("on_route_work", true);
        getViewState().openMainActivity();
    }
}
