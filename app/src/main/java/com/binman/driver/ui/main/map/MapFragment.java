package com.binman.driver.ui.main.map;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BaseFragment;
import com.binman.driver.models.Area;
import com.binman.driver.models.ContainersValue;
import com.binman.driver.ui.main.MainActivity;
import com.binman.driver.ui.main.list.ListFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.SENSOR_SERVICE;


public class MapFragment extends BaseFragment {

    @BindView(R.id.map) MapView mMapView;
    @BindView(R.id.listContainer) RecyclerView mListContainer;
    @BindView(R.id.number) TextView mNumber;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.address) TextView mAddress;
    @BindView(R.id.currentLoaded) TextView mCurrentLoaded;
    @BindView(R.id.rightContainer) View mRightContainer;
    @BindView(R.id.areasCount) TextView mAreasCount;
    private GoogleMap mMap;
    private ContainerAdapter mAdapter;
    private Marker my;
    private Area selected;

    private SensorManager mSensorManager;
    private Sensor magnetometer;
    private SensorEventListener mSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float degree = Math.round(event.values[0]);
            if (my != null)
                my.setRotation(degree - 90);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mSensorEventListener);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getActivity() != null)
            mSensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        return view;
    }

    @OnClick(R.id.allNotLoaded)
    void onClickAllNotLoaded() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).mPresenter.loadContainer(selected.getContainers(), 1, () -> {
                for (int i = 0; i < selected.getContainers().size(); i++) {
                    selected.getContainers().get(i).setStatus(1);
                }
                showArea(selected);
            });
    }

    @OnClick(R.id.allLoaded)
    void onClickAllLoaded() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).mPresenter.loadContainer(selected.getContainers(), 2, () -> {
                for (int i = 0; i < selected.getContainers().size(); i++) {
                    selected.getContainers().get(i).setStatus(2);
                }
                showArea(selected);
            });
    }

    @OnClick(R.id.layers)
    void onClickLayers() {
        int mapType = mMap.getMapType();
        if (mapType == GoogleMap.MAP_TYPE_NORMAL) {
            mapType = GoogleMap.MAP_TYPE_SATELLITE;
        } else {
            mapType = GoogleMap.MAP_TYPE_NORMAL;
        }
        mMap.setMapType(mapType);
    }

    @OnClick(R.id.minus)
    void onClickMinus() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMap.getCameraPosition().target, mMap.getCameraPosition().zoom - 1));
    }

    @OnClick(R.id.plus)
    void onClickPlus() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMap.getCameraPosition().target, mMap.getCameraPosition().zoom + 1));
    }

    @OnClick(R.id.gps)
    void onClickGps() {
        if (my != null)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(my.getPosition(), mMap.getCameraPosition().zoom));
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null)
            MapsInitializer.initialize(getActivity());
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        mMapView.getMapAsync(googleMap -> {
            mMap = googleMap;
            mMap.setMaxZoomPreference(18);
            mMap.setMinZoomPreference(11);
            mMap.setOnMarkerClickListener(marker -> {
                Area area = (Area) marker.getTag();
                if (getActivity() != null && area != null)
                    ((MainActivity) getActivity()).mPresenter.setExpand(area.getIndex());
                googleMap.clear();
                showPoints(((MainActivity) getActivity()).mPresenter.getAreas());

                return true;
            });
            if (getActivity() != null)
                showPoints(((MainActivity) getActivity()).mPresenter.getAreas());
        });
    }

    private void showArea(Area area) {
        mRightContainer.setVisibility(View.VISIBLE);
        mNumber.setText(String.valueOf(area.getIndex() + 1));
        mName.setText(area.getName());
        mAddress.setText(area.getAddress());
        ContainersValue containersValue = area.getContainersValue();
        mCurrentLoaded.setText(String.format(getResources().getString(R.string.loaded_placeholder), containersValue.loadedCount, containersValue.count));

        mAdapter = new ContainerAdapter(area.getContainers());
        mAdapter.setListener((containers, status, callBackListener) -> {
            if (getActivity() != null)
                ((MainActivity) getActivity()).mPresenter.loadContainer(containers, status, callBackListener);
        });

        mAdapter.setIDataChangeListener(() -> {
            if (selected != null) {
                ContainersValue containersValue1 = selected.getContainersValue();
                mCurrentLoaded.setText(String.format(getResources().getString(R.string.loaded_placeholder), containersValue1.loadedCount, containersValue1.count));
            }
        });
        mListContainer.setAdapter(mAdapter);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && mMap != null) {
            mMap.clear();
            if (getActivity() != null)
                showPoints(((MainActivity) getActivity()).mPresenter.getAreas());
        }
    }

    @OnClick(R.id.list)
    void onClickList() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).changeFragment(new ListFragment(), "ListFragment");
    }

    public void setMyPosition(double lat, double lon) {
        try {
            if (lat != 0.0) {
                LatLng latLng = new LatLng(lat, lon);
                if (my == null) {
                    my = mMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f));
                    my.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.my_marker));
                } else
                    my.setPosition(latLng);
            }
        } catch (Exception ignored) {

        }
    }

    public void notifyList() {
        mAdapter.notifyDataSetChanged();
    }

    private void showPoints(List<Area> areas) {
        mAreasCount.setText(String.format(getResources().getString(R.string.areas_count), areas.size()));
        mRightContainer.setVisibility(View.GONE);
        if (mMap == null) {
            return;
        }

        mMap.clear();

        if (areas.size() == 0) {
            return;
        }

        if (App.getApp().lon != 0.0) {
            LatLng latLng = new LatLng(App.getApp().lat, App.getApp().lon);
            my = mMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f));
            my.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.my_marker));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom));
        }


        for (int i = 0; i < areas.size(); i++) {
            LatLng latLng = new LatLng(areas.get(i).getLat(), areas.get(i).getLon());
            new LatLngBounds.Builder().include(latLng);
            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).anchor(1f, 1.1f));
            marker.setIcon(getMarker(areas.get(i).getStatus(), i, areas.get(i).isExpand()));
            if (areas.get(i).isExpand()) {
                selected = areas.get(i);
            }
            if (areas.get(i).isExpand()) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom));
                showArea(areas.get(i));
            }
            areas.get(i).setIndex(i);
            marker.setTag(areas.get(i));
        }
    }

    @SuppressLint("InflateParams")
    private BitmapDescriptor getMarker(int status, int id, boolean selected) {
        Paint paint = new Paint();

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        paint.setColor(Color.WHITE);
        paint.setTextSize(13 * getResources().getDisplayMetrics().density);
        paint.setTextScaleX(1.f);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        int idResource = R.drawable.marker_gray;
        if (status == 1)
            idResource = R.drawable.marker_red;
        if (status == 2)
            idResource = R.drawable.marker_green;
        if (status == 3)
            idResource = R.drawable.marker_orange;
        if (selected)
            idResource = R.drawable.marker_blue;


        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View markerView = layoutInflater.inflate(R.layout.item_marker, null, false);
        ((ImageView) markerView.findViewById(R.id.icon)).setImageResource(idResource);
        ((TextView) markerView.findViewById(R.id.index)).setText(String.valueOf(id + 1));
        markerView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Bitmap b = Bitmap.createBitmap(markerView.getMeasuredWidth(), markerView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        markerView.layout(0, 0, markerView.getMeasuredWidth(), markerView.getMeasuredHeight());
        markerView.draw(c);

        return BitmapDescriptorFactory.fromBitmap(b);
    }
}
