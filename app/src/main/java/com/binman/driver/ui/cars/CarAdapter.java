package com.binman.driver.ui.cars;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binman.driver.R;
import com.binman.driver.base.BaseRecyclerAdapter;
import com.binman.driver.models.Car;
import com.binman.driver.models.User;
import com.bumptech.glide.Glide;

import java.util.List;

public class CarAdapter extends BaseRecyclerAdapter<Car> {

    private IClickListener mListener;

    CarAdapter(List<Car> cars) {
        items = cars;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new CarAdapter.CarViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_car, parent, false));
            case 2:
                return new CarAdapter.ShowMoreViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_show_more, parent, false));
            default:
                return new CarAdapter.ProgressViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CarAdapter.CarViewHolder)
            ((CarAdapter.CarViewHolder) holder).bindView(items.get(position));
    }

    public class ShowMoreViewHolder extends RecyclerView.ViewHolder {

        ShowMoreViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> mListener.onClickShowMore());
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }


    public class CarViewHolder extends RecyclerView.ViewHolder {

        private ImageView mCarIcon;
        private ImageView mDriverIcon;
        private TextView mCarTitle;
        private TextView mCarSubtitle;
        private TextView mDriverName;
        private Button mSelectBtn;

        CarViewHolder(View itemView) {
            super(itemView);
            mCarIcon = itemView.findViewById(R.id.carIcon);
            mDriverIcon = itemView.findViewById(R.id.driverIcon);
            mCarTitle = itemView.findViewById(R.id.carTitle);
            mCarSubtitle = itemView.findViewById(R.id.carSubtitle);
            mDriverName = itemView.findViewById(R.id.driverName);
            mSelectBtn = itemView.findViewById(R.id.selectBtn);
        }

        void bindView(Car car) {
            if (car.getUser() != null) {
                Glide.with(mDriverIcon.getContext()).load(car.getUser().getImage()).into(mDriverIcon);
                mDriverName.setText(generateName(car.getUser()));
                mDriverName.setTextColor(mDriverName.getResources().getColor(R.color.active));
                mSelectBtn.setText(R.string.car_busy);
                mSelectBtn.setBackgroundResource(R.drawable.background_button_gray);
            } else {
                mSelectBtn.setBackgroundResource(R.drawable.background_button_blue);
                mSelectBtn.setText(R.string.select_car);
                mDriverIcon.setImageResource(R.drawable.not_assigned);
                mDriverName.setText(R.string.driver_not_assigned);
                mDriverName.setTextColor(mDriverName.getResources().getColor(R.color.noActive));
            }
            Glide.with(mCarIcon.getContext()).load(car.getImage()).into(mCarIcon);
            mCarTitle.setText(car.getName());
            mCarSubtitle.setText(car.getNumber());
            mSelectBtn.setOnClickListener(v -> {
                if (car.getUser() == null)
                    mListener.onClickSelectCar(car);
            });
        }
    }


    private String generateName(User user){
        String name = user.getLastname();
        if(user.getName() != null && user.getName().length() > 0){
            name = name + " " + user.getName().substring(0, 1) + ".";
        }

        if(user.getSecondname() != null && user.getSecondname().length() > 0){
            name = name + " " + user.getSecondname().substring(0,1);
        }
        return name;
    }
    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getTypeIndex();
    }

    protected interface IClickListener {
        void onClickSelectCar(Car car);

        void onClickShowMore();
    }

    void setListener(IClickListener listener) {
        mListener = listener;
    }
}