package com.binman.driver.ui.main.list;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binman.driver.R;
import com.binman.driver.base.BaseRecyclerAdapter;
import com.binman.driver.helpers.ViewHelper;
import com.binman.driver.models.Area;
import com.binman.driver.models.Container;
import com.binman.driver.models.ContainersValue;
import com.binman.driver.ui.main.IClickListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AreaAdapter extends BaseRecyclerAdapter<Area> {

    private IClickListener mListener;
    private IScrollCallBack mScrollCallBack;
    private IClickOnMap mClickOnMap;

    void setClickOnMap(IClickOnMap clickOnMap) {
        mClickOnMap = clickOnMap;
    }

    void setScrollCallBack(IScrollCallBack scrollCallBack) {
        mScrollCallBack = scrollCallBack;
    }

    AreaAdapter(List<Area> areas) {
        items = areas;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new AreaAdapter.ExpandAreaViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_area_expanced, parent, false));
        } else {
            return new AreaAdapter.AreaViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_area_collapsed, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (items.get(position).isExpand()) {
            ((AreaAdapter.ExpandAreaViewHolder) holder).bindView(position);
        } else {
            ((AreaAdapter.AreaViewHolder) holder).bindView(position);
        }
    }


    class AreaViewHolder extends RecyclerView.ViewHolder {

        View mainContainer;
        TextView mTitle;
        TextView mSubtitle;
        TextView mContainerValue;
        ImageView mIcon;
        TextView mNumber;
        ContainersValue containersValue;
        View mShowOnMap;
        View mContainerCount;
        ImageView mIconContainerStatus;

        AreaViewHolder(View itemView) {
            super(itemView);
            mainContainer = itemView.findViewById(R.id.main);
            mTitle = itemView.findViewById(R.id.title);
            mSubtitle = itemView.findViewById(R.id.subtitle);
            mContainerValue = itemView.findViewById(R.id.containerValue);
            mIcon = itemView.findViewById(R.id.icon);
            mNumber = itemView.findViewById(R.id.number);
            mShowOnMap = itemView.findViewById(R.id.showOnMap);
            mContainerCount = itemView.findViewById(R.id.containerCount);
            mIconContainerStatus = itemView.findViewById(R.id.iconContainerStatus);
        }

        @SuppressLint("DefaultLocale")
        void bindView(int position) {
            Area area = items.get(position);

            if (area.getStatus() == 1) {
                mContainerCount.setVisibility(View.VISIBLE);
                mIcon.setBackgroundResource(R.color.error);
                mIconContainerStatus.setImageResource(R.drawable.container_error);
                mContainerCount.setBackgroundResource(R.drawable.background_button_red);
            } else if (area.getStatus() == 2) {
                mContainerCount.setVisibility(View.VISIBLE);
                mIcon.setBackgroundResource(R.color.green);
                mIconContainerStatus.setImageResource(R.drawable.container);
                mContainerCount.setBackgroundResource(R.drawable.background_button_green);
            } else if (area.getStatus() == 3) {
                mContainerCount.setVisibility(View.VISIBLE);
                mIcon.setBackgroundResource(R.color.orange);
                mIconContainerStatus.setImageResource(R.drawable.container);
                mContainerCount.setBackgroundResource(R.drawable.background_button_orange);
            } else {
                mContainerCount.setVisibility(View.GONE);
                mIcon.setBackgroundResource(R.color.gray);
            }

            if (area.isExpand())
                mIcon.setImageResource(R.color.blue);

            mainContainer.setOnClickListener(v -> setSelected(position));
            mNumber.setText(String.valueOf(position + 1));
            mTitle.setText(area.getName());
            mSubtitle.setText(area.getAddress());
            containersValue = area.getContainersValue();
            mContainerValue.setText(String.format(mContainerValue.getResources().getString(R.string.containers_value_placeholder), containersValue.loadedCount, containersValue.count, String.format("%.1f", containersValue.loaded), String.format("%.2f", containersValue.volume)));
            mShowOnMap.setOnClickListener(v -> {
                setSelected(position);
                mClickOnMap.onCLickOnMap();
            });
        }
    }

    @SuppressLint("InflateParams")
    public class ExpandAreaViewHolder extends AreaViewHolder {

        LinearLayout mContainerContainers;

        ExpandAreaViewHolder(View itemView) {
            super(itemView);
            mContainerContainers = itemView.findViewById(R.id.containerContainers);
        }

        void bindView(int position) {
            super.bindView(position);
            Area area = items.get(position);

            mContainerContainers.removeAllViews();

            LayoutInflater layoutInflater = LayoutInflater.from(mContainerContainers.getContext());
            View viewTitle = layoutInflater.inflate(R.layout.item_containers_title, null, false);
            viewTitle.findViewById(R.id.allLoaded).setOnClickListener(v -> mListener.onClickLoad(area.getContainers(), 2, () -> {
                for (int i = 0; i < area.getContainers().size(); i++) {
                    area.getContainers().get(i).setStatus(2);
                }
                notifyDataSetChanged();
            }));
            viewTitle.findViewById(R.id.allNotLoaded).setOnClickListener(v -> mListener.onClickLoad(area.getContainers(), 1, () -> {
                for (int i = 0; i < area.getContainers().size(); i++) {
                    area.getContainers().get(i).setStatus(1);
                }
                notifyDataSetChanged();
            }));

            TextView currentState = viewTitle.findViewById(R.id.currentState);
            currentState.setText(String.format(currentState.getResources().getString(R.string.loaded_placeholder), containersValue.loadedCount, containersValue.count));
            mContainerContainers.addView(viewTitle);

            for (int i = 0; i < area.getContainers().size(); i++) {
                View item = layoutInflater.inflate(R.layout.item_container_area, null, false);
                ImageView iconContainer = item.findViewById(R.id.iconContainer);
                int finalI = i;
                ViewHelper.setButtonColor(item.findViewById(R.id.load), area.getContainers().get(i).getStatus(), true);
                ViewHelper.setButtonColor(item.findViewById(R.id.notLoad), area.getContainers().get(i).getStatus(), false);
                item.findViewById(R.id.load).setOnClickListener(v -> {
                    List<Container> cont = new ArrayList<>();
                    cont.add(area.getContainers().get(finalI));
                    mListener.onClickLoad(cont, 2, () -> {
                        items.get(position).getContainers().get(finalI).setStatus(2);
                        notifyDataSetChanged();
                    });
                });
                item.findViewById(R.id.notLoad).setOnClickListener(v -> {
                    List<Container> cont = new ArrayList<>();
                    cont.add(area.getContainers().get(finalI));
                    mListener.onClickLoad(cont, 1, () -> {
                        items.get(position).getContainers().get(finalI).setStatus(1);
                        notifyDataSetChanged();
                    });
                });
                Glide.with(iconContainer).load(area.getContainers().get(i).getImage()).into(iconContainer);
                TextView title = item.findViewById(R.id.title);
                title.setText(area.getContainers().get(i).getName());
                TextView subtitle = item.findViewById(R.id.subtitle);
                subtitle.setText(String.format(subtitle.getResources().getString(R.string.volume_placeholder), String.valueOf(area.getContainers().get(i).getVolume())));
                mContainerContainers.addView(item);
            }
        }
    }

    private void setSelected(int position) {
        Area area = items.get(position);
        if (area.isExpand()) {
            area.setExpand(false);
            notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < items.size(); i++) {

            items.get(i).setExpand(false);
        }
        area.setExpand(!area.isExpand());
        notifyDataSetChanged();
        mScrollCallBack.onScroll(position);
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).isExpand() ? 1 : 0;
    }


    public interface IScrollCallBack {
        void onScroll(int index);
    }

    public interface IClickOnMap {
        void onCLickOnMap();
    }

    public void setListener(IClickListener listener) {
        mListener = listener;
    }
}
