package com.binman.driver.ui.splash;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.base.BaseActivity;
import com.binman.driver.ui.welcome.WelcomeActivity;
import com.binman.driver.ui.login.LoginActivity;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashView {

    @InjectPresenter SplashPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);
        mPresenter.checkLogin();
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void openWelcomeActivity() {
        startActivity(new Intent(this, WelcomeActivity.class));
        finish();
    }
}
