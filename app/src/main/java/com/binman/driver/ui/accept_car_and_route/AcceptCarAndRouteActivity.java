package com.binman.driver.ui.accept_car_and_route;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.R;
import com.binman.driver.models.Car;
import com.binman.driver.models.Route;
import com.binman.driver.ui.menu.BaseMenuActivity;
import com.binman.driver.ui.cars.CarsActivity;
import com.binman.driver.ui.main.MainActivity;
import com.binman.driver.ui.routes.RoutesActivity;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AcceptCarAndRouteActivity extends BaseMenuActivity implements AcceptCarAndRouteView {

    @InjectPresenter AcceptCarAndRoutePresenter mPresenter;

    @BindView(R.id.carIcon) ImageView mCarIcon;
    @BindView(R.id.carTitle) TextView mCarTitle;
    @BindView(R.id.carSubtitle) TextView mCarSubtitle;
    @BindView(R.id.volume) TextView mVolume;
    @BindView(R.id.volumeLoaded) TextView mVolumeLoaded;
    @BindView(R.id.containerCount) TextView mContainersCount;
    @BindView(R.id.containerLoaded) TextView mContainersLoaded;
    @BindView(R.id.areasCount) TextView mAreasCount;
    @BindView(R.id.areasLoaded) TextView mAreasLoaded;
    @BindView(R.id.percent) TextView mPercent;
    @BindView(R.id.percentProgress) ProgressBar mPercentProgress;
    @BindView(R.id.containerProgress) ProgressBar mContainersProgress;
    @BindView(R.id.areasProgress) ProgressBar mAreasProgress;
    @BindView(R.id.progressVolume) ProgressBar mVolumeProgress;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.title) TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_car_and_route);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.load();
    }

    @OnClick(R.id.changeCar)
    protected void onCLickChangeCar() {
        mPresenter.changeCar();
    }

    @OnClick(R.id.changeRoute)
    protected void onClickChangeRoute() {
        startActivity(new Intent(this, RoutesActivity.class));
    }

    @Override
    public void openCarsActivity() {
        startActivity(new Intent(this, CarsActivity.class));
    }

    @Override
    public void showCar(Car car) {
        Glide.with(mCarIcon.getContext()).load(car.getImage()).into(mCarIcon);
        mCarTitle.setText(car.getName());
        mCarSubtitle.setText(car.getNumber());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showRoute(Route route) {
        mPercent.setText(route.getPercent() + "%");
        mPercentProgress.setProgress(Math.round(route.getPercent()));

        mVolume.setText(" / " + route.getVolume());
        mVolumeLoaded.setText(String.valueOf(route.getVolumeLoaded()));
        mVolumeProgress.setMax(Math.round(route.getVolume()));
        mVolumeProgress.setProgress(Math.round(route.getVolumeLoaded()));

        mContainersCount.setText(" / " + route.getContainersCount());
        mContainersLoaded.setText(String.valueOf(route.getContainersLoadedCount()));
        mContainersProgress.setMax(route.getContainersCount());
        mContainersProgress.setMax(route.getContainersLoadedCount());

        mAreasCount.setText(" / " + route.getAreasCount());
        mAreasLoaded.setText(String.valueOf(route.getAreasLoadedCount()));
        mAreasProgress.setMax(route.getAreasCount());
        mAreasProgress.setProgress(route.getAreasLoadedCount());
        mName.setText(route.getName());
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void showTitle(String name) {
        mTitle.setText(String.format(getResources().getString(R.string.accept_title_placeholder), name));
    }

    @OnClick(R.id.startWork)
    protected void onClickStartWork() {
        mPresenter.startWork();
    }
}
