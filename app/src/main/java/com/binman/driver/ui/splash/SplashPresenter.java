package com.binman.driver.ui.splash;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.models.User;

import javax.inject.Inject;

@InjectViewState
public class SplashPresenter extends BasePresenter<SplashView> {


    @Inject LocalStorage mLocalStorage;

    SplashPresenter() {
        App.getAppComponent().inject(this);
    }

    void checkLogin(){
        if (mLocalStorage.readObject("login", User.class) != null) {
            getViewState().openWelcomeActivity();
        } else {
            getViewState().openLoginActivity();
        }
    }
}
