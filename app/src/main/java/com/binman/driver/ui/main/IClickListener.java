package com.binman.driver.ui.main;

import com.binman.driver.models.Container;

import java.util.List;

public interface IClickListener {
    void onClickLoad(List<Container> containers, int status, ICallBackListener callBackListener);
}
