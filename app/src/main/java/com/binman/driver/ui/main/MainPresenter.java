package com.binman.driver.ui.main;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.helpers.ErrorHelper;
import com.binman.driver.helpers.RestartHelper;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.Area;
import com.binman.driver.models.Container;
import com.binman.driver.models.Result;
import com.binman.driver.models.Route;
import com.binman.driver.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {


    @Inject LocalStorage mLocalStorage;
    @Inject Api mApi;
    private List<Area> mAreas = new ArrayList<>();

    MainPresenter() {
        App.getAppComponent().inject(this);
    }

    public List<Area> getAreas() {
        return mAreas;
    }


    void load() {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("entity", "areas");
        fields.put("route_id", mLocalStorage.readObject("selected_route", Route.class).getId());

        getViewState().showProgress(true);
        Disposable disposable = mApi.loadAreas(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                getViewState().showProgress(false);
                                getViewState().showRoute(result.getRouteInfo());
                                mAreas.addAll(result.getAreasList());
                                getViewState().setAreasCount(result.getAreasList().size());
                                getViewState().notifyList();
                            } else if (result.getStatus() == 4) {
                                getViewState().showToast("Маршрут занят");
                            } else if (result.getStatus() == 3) {
                                getViewState().showToast("Машина занята");
                            } else if (result.getStatus() == 2) {
                                getViewState().showToast("Ошибка авторизации");
                                mLocalStorage.clearData();
                                RestartHelper.restartApp(App.getApp());
                            } else {
                                getViewState().showToast(R.string.unknown_exception);
                            }
                        }
                        , throwable -> {
                            getViewState().showProgress(false);
                            getViewState().showToast(throwable.getMessage());
                        });
        unsubscribeOnDestroy(disposable);
    }

    void driveLandfill(int status) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("polygon", status);
        fields.put("timestamp", Time.getCurrentTimeInSeconds());


        getViewState().showProgress(true);
        Disposable disposable = mApi.driveLandfill(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            getViewState().showProgress(false);
                            if (result.getStatus() == 0) {
                                getViewState().showOnLandfill(status == 1);
                                getViewState().notifyList();
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                        }
                        , throwable -> {
                            getViewState().showProgress(false);
                            getViewState().showToast(throwable.getMessage());
                        });
        unsubscribeOnDestroy(disposable);
    }

    void endRoute() {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("route_id", 0);
        fields.put("timestamp", Time.getCurrentTimeInSeconds());

        getViewState().showProgress(true);
        Disposable disposable = mApi.takeCar(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                mLocalStorage.writeBoolean("on_route_work", false);
                                getViewState().openRouteActivity();
                                getViewState().showProgress(false);
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                                getViewState().showProgress(false);
                            }
                        }
                        , throwable -> {
                            getViewState().showToast(throwable.getMessage());
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    void endDay() {
        Map<String, Object> fieldsRoute = new HashMap<>();
        fieldsRoute.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fieldsRoute.put("timestamp", Time.getCurrentTimeInSeconds());
        fieldsRoute.put("route_id", 0);

        Map<String, Object> fieldsWorkDay = new HashMap<>();
        fieldsWorkDay.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fieldsWorkDay.put("timestamp", Time.getCurrentTimeInSeconds());
        fieldsWorkDay.put("working", 0);

        getViewState().showProgress(true);

        List<Result> results = new ArrayList<>();
        Disposable disposable = mApi.selectRoute(fieldsRoute)
                .subscribeOn(Schedulers.newThread())
                .flatMap((Function<Result, ObservableSource<Result>>) result -> {
                    results.add(result);
                    return mApi.workDay(fieldsWorkDay);
                })
                .map(result -> {
                    results.add(result);
                    return result;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (results.size() == 2 && results.get(0).getStatus() == 0 && results.get(1).getStatus() == 0) {
                                mLocalStorage.writeBoolean("on_route_work", false);
                                getViewState().restartApp();
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }
                            getViewState().showProgress(false);
                        }
                        , throwable -> {
                            getViewState().showToast(throwable.getMessage());
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    public void loadContainer(List<Container> containers, int status, ICallBackListener callBackListener) {
        if (status == 1)
            getViewState().showConfirmLackLoadDialog(() -> loadConfirmedContainer(containers, status, callBackListener));
        else
            getViewState().showConfirmLoadDialog(() -> loadConfirmedContainer(containers, status, callBackListener));
    }

    private void loadConfirmedContainer(List<Container> containers, int status, ICallBackListener callBackListener) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("token", mLocalStorage.readObject("login", User.class).getToken());
        fields.put("route_id", mLocalStorage.readObject("selected_route", Route.class).getId());
        fields.put("timestamp", Time.getCurrentTimeInSeconds());
        fields.put("entity", "containers");
        fields.put("status", status);
        fields.put("lat", App.getApp().lat);
        fields.put("lon", App.getApp().lon);
        getViewState().showProgress(true);

        List<Long> ids = generateIdOfContainers(containers);

        Disposable disposable = mApi.loadContainers(fields, ids)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getStatus() == 0) {
                                callBackListener.onCallBack();
                                getViewState().showRoute(result);
                            } else {
                                ErrorHelper.checkError(mLocalStorage, result.getStatus(), getViewState());
                            }

                            getViewState().showProgress(false);
                        }
                        , throwable -> {
                            getViewState().showToast(throwable.getMessage());
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    private List<Long> generateIdOfContainers(List<Container> containers) {
        List<Long> ids = new ArrayList<>();
        for (Container container : containers) {
            ids.add(container.getId());
        }
        return ids;
    }

    public void setExpand(int index) {
        for (int i = 0; i < mAreas.size(); i++) {
            mAreas.get(i).setExpand(false);
        }
        mAreas.get(index).setExpand(true);
        getViewState().notifyList();
        getViewState().scrollToPosition(index);
    }

    void showTurnOnGPS(boolean providerEnabled) {
        getViewState().showTurnOnGPS(providerEnabled);
    }
}
