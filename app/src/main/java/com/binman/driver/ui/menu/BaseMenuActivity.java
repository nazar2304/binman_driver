package com.binman.driver.ui.menu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.binman.driver.App;
import com.binman.driver.R;
import com.binman.driver.base.BaseActivity;
import com.binman.driver.base.BaseView;
import com.binman.driver.dagger.module.LocalStorage;
import com.binman.driver.helpers.RestartHelper;
import com.binman.driver.models.User;
import com.binman.driver.ui.about.AboutActivity;
import com.binman.driver.ui.main.MainActivity;
import com.binman.driver.ui.routes.RoutesActivity;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.OnClick;

@SuppressLint("Registered")
public class BaseMenuActivity extends BaseActivity implements BaseView {

    @Inject LocalStorage mLocalStorage;
    View menuDialog;
    ImageView menu;

    @Override
    protected void onStart() {
        super.onStart();
        App.getAppComponent().inject(this);

        menu = findViewById(R.id.menu);
        ImageView mAvatar = findViewById(R.id.avatarMenu);
        menuDialog = findViewById(R.id.menuDialog);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuDialog.setVisibility(View.VISIBLE == menuDialog.getVisibility() ? View.GONE : View.VISIBLE);
                menu.setImageResource(View.VISIBLE == menuDialog.getVisibility() ? R.drawable.close : R.drawable.menu);
            }
        });
        TextView mName = findViewById(R.id.nameMenu);
        TextView mEmail = findViewById(R.id.emailMenu);
        mName.setText(mLocalStorage.readObject("login", User.class).getLastname() + " " + mLocalStorage.readObject("login", User.class).getName() + " " + mLocalStorage.readObject("login", User.class).getSecondname());
        mEmail.setText(mLocalStorage.readObject("login", User.class).getEmail());
        Glide.with(this).load(mLocalStorage.readObject("login", User.class).getImage()).into(mAvatar);
        findViewById(R.id.backToWorkContainer).setVisibility(mLocalStorage.readBoolean("on_route_work") ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.exit)
    void onClickExit() {
        mLocalStorage.clearData();
        RestartHelper.restartApp(this);
    }

    @OnClick(R.id.routes)
    void onClickRoutes() {
        menuDialog.setVisibility(View.GONE);
        menu.setImageResource(R.drawable.menu);
        startActivity(new Intent(this, RoutesActivity.class));
    }

    @OnClick(R.id.back)
    void onClickBack() {
        menuDialog.setVisibility(View.GONE);
        menu.setImageResource(R.drawable.menu);
        if (mLocalStorage.readBoolean("on_route_work"))
            if (!(this instanceof MainActivity))
                startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.about)
    void onClickAbout() {
        menuDialog.setVisibility(View.GONE);
        menu.setImageResource(R.drawable.menu);
        if (!(this instanceof AboutActivity))
            startActivity(new Intent(this, AboutActivity.class));
    }

    @OnClick(R.id.reportBug)
    void onClickReportBug() {
        String mailto = "mailto:support@binman.ru?subject=Binman.Навигатор водителя";
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));
        startActivity(emailIntent);
    }
}
