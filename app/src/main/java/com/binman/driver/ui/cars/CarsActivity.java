package com.binman.driver.ui.cars;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.R;
import com.binman.driver.models.Car;
import com.binman.driver.ui.menu.BaseMenuActivity;
import com.binman.driver.ui.accept_car_and_route.AcceptCarAndRouteActivity;
import com.binman.driver.ui.routes.RoutesActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsActivity extends BaseMenuActivity implements CarsView {

    @BindView(R.id.list) RecyclerView mList;
    @BindView(R.id.carsCount) TextView mCarsCount;
    private CarAdapter mAdapter;

    @InjectPresenter CarsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_cars);
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);

        mAdapter = new CarAdapter(mPresenter.getCars());
        mAdapter.setListener(new CarAdapter.IClickListener() {
            @Override
            public void onClickSelectCar(Car car) {
                mPresenter.selectCar(car);
            }

            @Override
            public void onClickShowMore() {
                mPresenter.load();
            }
        });
        mList.setAdapter(mAdapter);
        mPresenter.load();
    }


    @Override
    public void notifyList() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setCarsCount(int carsCount) {
        mCarsCount.setText(String.format(getResources().getString(R.string.cars_count), carsCount));
    }

    @Override
    public void openRoutes() {
        startActivity(new Intent(this, RoutesActivity.class));
    }

    @Override
    public void openAcceptCarAndRoute() {
        startActivity(new Intent(this, AcceptCarAndRouteActivity.class));
    }
}
