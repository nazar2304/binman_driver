package com.binman.driver.ui.cars;

import com.binman.driver.base.BaseView;

public interface CarsView extends BaseView {

    void notifyList();

    void setCarsCount(int carsCount);

    void openRoutes();

    void openAcceptCarAndRoute();
}
