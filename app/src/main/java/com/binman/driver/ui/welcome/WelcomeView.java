package com.binman.driver.ui.welcome;

import com.binman.driver.base.BaseView;

public interface WelcomeView extends BaseView {

    void openLoginActivity();

    void showUserName(String name);

    void openCarsActivity();

    void openMainActivity();
}
