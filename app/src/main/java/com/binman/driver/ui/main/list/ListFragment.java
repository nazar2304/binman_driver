package com.binman.driver.ui.main.list;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binman.driver.R;
import com.binman.driver.base.BaseFragment;
import com.binman.driver.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListFragment extends BaseFragment {

    @BindView(R.id.list) RecyclerView mList;
    @BindView(R.id.areasCount) TextView mAreasCount;
    private AreaAdapter mAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            mAdapter = new AreaAdapter(((MainActivity) getActivity()).mPresenter.getAreas());
            mAdapter.setListener((containers, status, callBackListener) -> {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).mPresenter.loadContainer(containers, status, callBackListener);
            });
            mAdapter.setScrollCallBack(index -> {
                if (mList.getLayoutManager() != null)
                    mList.getLayoutManager().scrollToPosition(index);
            });
            mAdapter.setClickOnMap(() -> {
                if (getActivity() != null)
                    ((MainActivity) getActivity()).showMapFragment();
            });
            mList.setAdapter(mAdapter);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden && mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.map)
    void onClickMap() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).showMapFragment();
    }

    public void notifyList() {
        mAdapter.notifyDataSetChanged();
    }

    public void scrollToPosition(int position) {
        if (mList.getLayoutManager() != null)
            mList.getLayoutManager().scrollToPosition(position);
    }

    public void setCount(int count) {
        mAreasCount.setText(String.format(getResources().getString(R.string.areas_count), count));
    }
}
