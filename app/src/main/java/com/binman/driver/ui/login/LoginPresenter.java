package com.binman.driver.ui.login;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.binman.driver.App;
import com.binman.driver.base.BasePresenter;
import com.binman.driver.connection.Api;
import com.binman.driver.dagger.module.LocalStorage;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {
    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    LoginPresenter() {
        App.getAppComponent().inject(this);
    }

    public void login(String email, String pass) {

        Map<String, String> fields = new HashMap<>();
        fields.put("login", email);
        fields.put("password", pass);

        getViewState().showProgress(true);
        Disposable disposable = mApi.login(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (!TextUtils.isEmpty(result.getName())) {
                                mLocalStorage.writeObject("login", result);
                                getViewState().showErrorMsg(false);
                                getViewState().openMainActivity();

                            } else {
                                getViewState().showErrorMsg(true);
                            }
                            getViewState().showProgress(false);
                        }
                        , throwable -> {
                            getViewState().showProgress(false);
                            getViewState().showErrorMsg(true);
                        });

        unsubscribeOnDestroy(disposable);
    }
}
