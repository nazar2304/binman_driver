package com.binman.driver.ui.main;

public interface IConfirmListener {
    void onConfirm();
}
