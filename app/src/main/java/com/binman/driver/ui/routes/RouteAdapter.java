package com.binman.driver.ui.routes;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binman.driver.R;
import com.binman.driver.base.BaseRecyclerAdapter;
import com.binman.driver.helpers.Time;
import com.binman.driver.models.Route;
import com.binman.driver.models.User;

import java.util.List;

public class RouteAdapter extends BaseRecyclerAdapter<Route> {

    private IClickListener mListener;

    RouteAdapter(List<Route> cars) {
        items = cars;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new RouteAdapter.RouteViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_route, parent, false));
            case 2:
                return new RouteAdapter.ShowMoreViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_show_more, parent, false));
            default:
                return new RouteAdapter.ProgressViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RouteAdapter.RouteViewHolder)
            ((RouteAdapter.RouteViewHolder) holder).bindView(items.get(position));
    }

    public class ShowMoreViewHolder extends RecyclerView.ViewHolder {

        ShowMoreViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> mListener.onClickShowMore());
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder {

        TextView mVolume;
        TextView mVolumeLoaded;
        TextView mContainersCount;
        TextView mContainersLoaded;
        TextView mAreasCount;
        TextView mAreasLoaded;
        TextView mPercent;
        ProgressBar mPercentProgress;
        ProgressBar mContainersProgress;
        ProgressBar mAreasProgress;
        ProgressBar mVolumeProgress;

        TextView mName;
        TextView mRouteCurrentDistance;
        TextView mTime;

        View mContainerTime;
        View mContainerDistance;
        View mContainerDriver;
        View mSelectRoute;
        View mNoWork;

        TextView mDriver;
        TextView mCar;


        RouteViewHolder(View itemView) {
            super(itemView);
            mVolume = itemView.findViewById(R.id.volume);
            mVolumeLoaded = itemView.findViewById(R.id.volumeLoaded);
            mContainersCount = itemView.findViewById(R.id.containerCount);
            mContainersLoaded = itemView.findViewById(R.id.containerLoaded);
            mAreasCount = itemView.findViewById(R.id.areasCount);
            mAreasLoaded = itemView.findViewById(R.id.areasLoaded);
            mPercent = itemView.findViewById(R.id.percent);

            mPercentProgress = itemView.findViewById(R.id.percentProgress);
            mContainersProgress = itemView.findViewById(R.id.containerProgress);
            mAreasProgress = itemView.findViewById(R.id.areasProgress);
            mVolumeProgress = itemView.findViewById(R.id.progressVolume);

            mName = itemView.findViewById(R.id.name);
            mRouteCurrentDistance = itemView.findViewById(R.id.routeCurrentDistance);
            mTime = itemView.findViewById(R.id.time);

            mContainerTime = itemView.findViewById(R.id.containerTime);
            mContainerDistance = itemView.findViewById(R.id.containerDistance);
            mContainerDriver = itemView.findViewById(R.id.containerDriver);
            mSelectRoute = itemView.findViewById(R.id.selectRoute);
            mNoWork = itemView.findViewById(R.id.noWork);
            mDriver = itemView.findViewById(R.id.driver);
            mCar = itemView.findViewById(R.id.car);

        }

        @SuppressLint("SetTextI18n")
        void bindView(Route route) {
            mPercent.setText(route.getPercent() + " %");
            mPercentProgress.setProgress(Math.round(route.getPercent()));

            mVolume.setText(" / " + route.getVolume());
            mVolumeLoaded.setText(String.valueOf(route.getVolumeLoaded()));
            mVolumeProgress.setMax(Math.round(route.getVolume()));
            mVolumeProgress.setProgress(Math.round(route.getVolumeLoaded()));

            mContainersCount.setText(" / " + route.getContainersCount());
            mContainersLoaded.setText(String.valueOf(route.getContainersLoadedCount()));
            mContainersProgress.setMax(route.getContainersCount());
            mContainersProgress.setMax(route.getContainersLoadedCount());

            mAreasCount.setText(" / " + route.getAreasCount());
            mAreasLoaded.setText(String.valueOf(route.getAreasLoadedCount()));
            mAreasProgress.setMax(route.getAreasCount());
            mAreasProgress.setProgress(route.getAreasLoadedCount());

            mRouteCurrentDistance.setText(route.getRun());
            mName.setText(route.getName());
            mTime.setText(Time.convertSecondsToTime(route.getTime()));

            mContainerDistance.setVisibility(route.getTime() == 0 ? View.GONE : View.VISIBLE);
            mContainerTime.setVisibility(route.getTime() == 0 ? View.GONE : View.VISIBLE);
            mNoWork.setVisibility(route.getTime() == 0 ? View.VISIBLE : View.GONE);
            mContainerDriver.setVisibility(route.getDrivers().size() > 0 ? View.VISIBLE : View.GONE);
            mSelectRoute.setVisibility(route.getDrivers().size() > 0 ? View.GONE : View.VISIBLE);

            if (route.getDrivers().size() > 0) {
                User user = route.getDrivers().get(0);
                mDriver.setText(user.getLastname() + " " + user.getName() + " " + user.getSecondname());
                if (user.getCar() != null)
                    mCar.setText(user.getCar().getName());
            }

            mSelectRoute.setOnClickListener(v -> mListener.onClickSelectRoute(route));
        }
    }


    protected interface IClickListener {
        void onClickSelectRoute(Route route);

        void onClickShowMore();
    }

    void setListener(RouteAdapter.IClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().getTypeIndex();
    }
}
