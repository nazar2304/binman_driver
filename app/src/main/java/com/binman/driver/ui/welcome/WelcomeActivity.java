package com.binman.driver.ui.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.binman.driver.R;
import com.binman.driver.base.BaseActivity;
import com.binman.driver.ui.cars.CarsActivity;
import com.binman.driver.ui.login.LoginActivity;
import com.binman.driver.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity implements WelcomeView {

    @InjectPresenter WelcomePresenter mPresenter;
    @BindView(R.id.welcomeText) TextView mWelcomeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        unbinder = ButterKnife.bind(this);
        mPresenter.load();
    }

    @OnClick(R.id.start)
    protected void onClickStart() {
        mPresenter.startWorkDay();
    }

    @OnClick(R.id.exit)
    protected void onClickExit() {
        mPresenter.clickExit();
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void showUserName(String name) {
        mWelcomeText.setText(String.format(getResources().getString(R.string.welcome_text), name.trim()));
    }

    @Override
    public void openCarsActivity() {
        startActivity(new Intent(this, CarsActivity.class));
        finish();
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
