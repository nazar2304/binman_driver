package com.binman.driver.ui.main;

import com.binman.driver.base.BaseView;
import com.binman.driver.models.Route;

public interface MainView extends BaseView {

    void showRoute(Route route);

    void notifyList();

    void scrollToPosition(int position);

    void setAreasCount(int count);

    void showOnLandfill(boolean status);

    void openRouteActivity();

    void restartApp();

    void showTurnOnGPS(boolean status);

    void showConfirmLoadDialog(IConfirmListener confirmListener);

    void showConfirmLackLoadDialog(IConfirmListener confirmListener);
}
