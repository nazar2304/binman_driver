package com.binman.driver.connection;

import com.binman.driver.models.Result;
import com.binman.driver.models.ResultAreas;
import com.binman.driver.models.ResultCars;
import com.binman.driver.models.ResultRoutes;
import com.binman.driver.models.Route;
import com.binman.driver.models.User;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

import static com.binman.driver.connection.ApiConstant.API_ADDRESS;


public interface Api {

    @GET(API_ADDRESS)
    Observable<User> login(@QueryMap Map<String, String> fields);


    @GET(API_ADDRESS)
    Observable<ResultCars> getCars(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Result> takeCar(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<ResultRoutes> getRoutes(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Result> workDay(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<ResultAreas> loadAreas(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Result> driveLandfill(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Result> selectRoute(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Result> sendPoint(@QueryMap Map<String, Object> fields);

    @GET(API_ADDRESS)
    Observable<Route> loadContainers(@QueryMap Map<String, Object> fields, @Query(value ="id[]" , encoded=true) List<Long> id);
}
