package com.binman.driver;

import android.app.Application;
import android.util.Log;

import com.binman.driver.dagger.AppComponent;
import com.binman.driver.dagger.DaggerAppComponent;
import com.binman.driver.dagger.module.ContextModule;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;

import io.fabric.sdk.android.Fabric;

public class App extends Application {
    private static AppComponent sAppComponent;
    private static App app;
    public double lon;
    public double lat;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());
        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        app = this;
    }

    public void setLatLon(double lat, double lon){
        this.lat = lat;
        this.lon = lon;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static App getApp() {
        return app;
    }

}
