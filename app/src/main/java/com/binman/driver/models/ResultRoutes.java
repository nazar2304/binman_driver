package com.binman.driver.models;

import java.util.List;

public class ResultRoutes extends Result {

    private Long allCount;
    private List<Route> routes = null;

    public Long getAllCount() {
        return allCount;
    }

    public void setAllCount(Long allCount) {
        this.allCount = allCount;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}