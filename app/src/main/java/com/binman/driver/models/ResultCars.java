package com.binman.driver.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultCars extends Result {

    @SerializedName("allCount")
    @Expose
    private int allCount;
    @SerializedName("cars_list")
    @Expose
    private List<Car> carsList = null;

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }

    public List<Car> getCarsList() {
        return carsList;
    }

    public void setCarsList(List<Car> carsList) {
        this.carsList = carsList;
    }

}
