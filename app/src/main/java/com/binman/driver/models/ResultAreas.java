package com.binman.driver.models;

import java.util.List;

public class ResultAreas extends Result {

    private Route route_info;
    private List<Area> areas_list = null;

    public Route getRouteInfo() {
        return route_info;
    }

    public List<Area> getAreasList() {
        return areas_list;
    }

    public void setAreasList(List<Area> areasList) {
        this.areas_list = areasList;
    }
}