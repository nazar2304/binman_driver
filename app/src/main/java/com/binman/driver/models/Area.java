package com.binman.driver.models;

import java.util.ArrayList;
import java.util.List;

public class Area {

    private Long id;
    private int index;
    private String name;
    private Double lat;
    private Double lon;
    private String address;
    private List<Container> containers = null;
    private boolean isExpand;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Container> getContainers() {
        if(containers == null)
            containers = new ArrayList<>();
        return containers;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setContainers(List<Container> containers) {
        this.containers = containers;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public int getStatus(){
        List<Integer> status = new ArrayList<>();

        for(int i = 0; i < getContainers().size(); i++){
            if(!status.contains(new Integer(getContainers().get(i).getStatus()))) {
                status.add(getContainers().get(i).getStatus());
            }
        }
        return status.size() == 1 ? status.get(0) : status.size() == 0 ? 0 : 3;
    }

    public ContainersValue getContainersValue() {
        ContainersValue containersValue = new ContainersValue();
        for (int i = 0; i < getContainers().size(); i++) {
            if (getContainers().get(i).getStatus() == 2) {
                containersValue.loaded = containersValue.loaded + getContainers().get(i).getVolume();
                containersValue.loadedCount++;
            }
            containersValue.volume = containersValue.volume + getContainers().get(i).getVolume();
            containersValue.count++;
        }
        return containersValue;
    }
}