package com.binman.driver.models;

public enum Type {
    STANDART(1), SHOW_MORE(2), PROGRESS(3);

    int typeIndex;

    Type(int typeIndex) {
        this.typeIndex = typeIndex;
    }

    public int getTypeIndex() {
        return typeIndex;
    }
}
