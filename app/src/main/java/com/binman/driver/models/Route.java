package com.binman.driver.models;

import java.util.ArrayList;
import java.util.List;

public class Route {

    private Type type = Type.STANDART;

    private long id;
    private Float percent;
    private String name;
    private int areas_count;
    private int areas_loaded_count;
    private int containers_count;
    private int containers_loaded_count;
    private Float volume;
    private Float volumeLoaded;
    private long time;
    private String run;
    private List<User> drivers;
    private int status;

    public int getStatus() {
        return status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAreasCount() {
        return areas_count;
    }

    public void setAreasCount(int areasCount) {
        this.areas_count = areasCount;
    }

    public int getAreasLoadedCount() {
        return areas_loaded_count;
    }

    public void setAreasLoadedCount(int areasLoadedCount) {
        this.areas_loaded_count = areasLoadedCount;
    }

    public int getContainersCount() {
        return containers_count;
    }

    public void setContainersCount(int containersCount) {
        this.containers_count = containersCount;
    }

    public int getContainersLoadedCount() {
        return containers_loaded_count;
    }

    public void setContainersLoadedCount(int containersLoadedCount) {
        this.containers_loaded_count = containersLoadedCount;
    }

    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    public Float getVolumeLoaded() {
        return volumeLoaded;
    }

    public void setVolumeLoaded(Float volumeLoaded) {
        this.volumeLoaded = volumeLoaded;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public List<User> getDrivers() {
        if(drivers == null){
            drivers = new ArrayList<>();
        }
        return drivers;
    }

    public void setDrivers(List<User> drivers) {
        this.drivers = drivers;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}